# multidimensional n-body performance
## What is it?
The aim of this project is to
have a setting to analyze the performance of a non-trivial algorithm
depending on the memory layout used.
The code is written to be as simple as possible:
you must not need to have expert knowledge in C++ to write the same code.
It tries to highlight compilers capabilites like auto-vectorization

## How to compile it?
This project use make for compiling.
You can pass some options to make in order to control the compilation:
```sh
make OPTION1_NAME=option1_value OPTION2_NAME=option2_value ...
```
Here are the existing options:
- `DIM`: set the number of dimensions used (default = 2, appends `-xd` to the program name where x is the number of dimension)
- `COL`: if not 0, simulates collisions betweens bodies (EXPERIMENTAL, default = 0, appends `_col` to the program name when enabled)
- `MLAYOUT`: set the memory layout used. Possible values:
    - `aos`: Array of Structures (default)
    - `aosp`: Array of pointers to Structure (appends `_aosp` to the program name)
    - `soa`: Structure of Arrays (appends `_soa` to the program name)
    - `soac`: SOAContainer: class to easily write soa code  (appends `_soca` to the program name)
    - `aosoa`: SOAContainer: class to easily write soa code  (appends `_aosoa` to the program name)
- `DOUBLE`: if not 0, use double precision for calculus (default = 0, appends `_f64` to the program name when enabled)
- `OPENMP`: if not 0, enables OpenMP (NOT IMPLEMENTED, default = 0, appends `_openmp` to the program name when enabled)
- `VEC`: if not 0, enables the vectorization (default = 1, appends `_novec` to the program name when disabled)
- `DEBUG`: if not 0, compiles in debug mode (default = 0, appends `_dbg` to the program name when enabled)

The program name is `n-body` and suffixes are appended following the order of the previous list.
The program is located in the `exe/` directory.

You may need to create the directories `exe/` and `obj/` before the first compilation.

## How to execute it?
The program can be executed using the following commandline:
```sh
exe/n-body-xd [n_bodies=100 [n_iterations=100]]
```
Let's assume I want to execute my program in 2 dimensions with 60 bodies and 200 iterations.
I can do it executing the following commandline:
```sh
exe/n-body-2d 60 200
```

## Sample output
```
Memory Layout: aos
100 points   100 iterations   2 dimensions
     0.00328435    sec
    3.28435e-05    sec.iter⁻¹
    3.28435e-09    sec.iter⁻¹.el⁻²
    1.64218e-09    sec.iter⁻¹.el⁻².dim⁻¹
```