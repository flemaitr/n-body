# -------------------- #
# -- poiAV Makefile -- #
# -------------------- #

# -- Lile list ----------
FILE = main.cpp

# -- Paths ----------
SRC_PATH = src
OBJ_PATH = obj
EXE_PATH = exe
INC_PATH = include

LIB_LIB_PATH = -lrt -static-libgcc -static-libstdc++
LIB_INC_PATH = -ISOAContainer/include

# -- Macros ----------
CC = g++
#CC = icpc
AR = ar -rc

# -- Make arguments ----------
ifndef OPENMP
	OPENMP = 0
endif
ifndef DOUBLE
	DOUBLE = 0
endif
ifndef VEC
	VEC = 1
endif
ifndef DEBUG
	DEBUG = 0
endif
ifndef MLAYOUT
	MLAYOUT = aos
endif
ifndef DIM
	DIM = 2
endif
ifndef COL
	COL = 0
endif

DIM_SUFFIX = -$(DIM)d
ifeq ($(MLAYOUT),aos)
	MLAYOUT_SUFFIX = 
else
	MLAYOUT_SUFFIX = _$(MLAYOUT)
endif
ifneq ($(if $(COL),$(strip $(COL)),0),0)
	COL_SUFFIX = _col
else
	COL_SUFFIX = 
endif
ifneq ($(if $(OPENMP),$(strip $(OPENMP)),0),0)
	OPENMP_SUFFIX = _omp
else
	OPENMP_SUFFIX = 
endif
ifneq ($(if $(DOUBLE),$(strip $(DOUBLE)),0),0)
	DOUBLE_SUFFIX = _f64
else
	DOUBLE_SUFFIX = 
endif
ifeq ($(if $(VEC),$(strip $(VEC)),0),0)
	VEC_SUFFIX = _novec
else
	VEC_SUFFIX = 
endif
ifneq ($(if $(DEBUG),$(strip $(DEBUG)),0),0)
	DEBUG_SUFFIX = _dbg
else
	DEBUG_SUFFIX = 
endif

SUFFIX = $(DIM_SUFFIX)$(COL_SUFFIX)$(MLAYOUT_SUFFIX)$(DOUBLE_SUFFIX)$(OPENMP_SUFFIX)$(VEC_SUFFIX)$(DEBUG_SUFFIX)

# -- Flags ----------
ifneq ($(if $(VEC),$(strip $(VEC)),0),0)
	C_VEC_FLAGS = -ftree-vectorize -fopt-info-vec
else
	C_VEC_FLAGS = -fno-tree-vectorize
endif
C_FP_FLAGS = -fassociative-math -fno-math-errno -fno-trapping-math -fno-signed-zeros -fno-signaling-nans
#C_FP_FLAGS = -fassociative-math -fno-trapping-math -fno-signed-zeros -fno-math-errno -ffinite-math-only -fno-rounding-math -fno-signaling-nans
C_DEBUG_FLAGS = -std=c++11 -O0 -g -ftemplate-depth=4096
#C_DEBUG_FLAGS = -std=c++1y -O3 -g -fstrict-aliasing $(C_VEC_FLAGS) $(C_FP_FLAGS) -ftemplate-depth=4096
C_OPTIMISATION_FLAGS = -std=c++1y -O3 -fstrict-aliasing $(C_VEC_FLAGS) $(C_FP_FLAGS) -ftemplate-depth=4096
#C_OPTIMISATION_FLAGS = -std=c++1y -O3 -fstrict-aliasing -ftemplate-depth=4096 -vec -vec-report -prec-div -prec-sqrt -fp-model=precise -ftz
ifneq ($(if $(OPENMP),$(strip $(OPENMP)),0),0)
C_OPENMP_FLAGS = -DOPENMP -fopenmp
endif
ifneq ($(if $(DOUBLE),$(strip $(DOUBLE)),0),0)
C_DOUBLE_FLAGS = -DDOUBLE
endif
ifneq ($(if $(COL),$(strip $(COL)),0),0)
C_COL_FLAGS = -DCOL
endif
C_ARCH_FLAGS = -march=native
C_INC_FLAGS = -I$(INC_PATH)

# -- Options ----------
C_OPTIONS_FLAGS = -DDIM=$(DIM) -DMLAYOUT=$(MLAYOUT) $(C_DOUBLE_FLAGS) $(C_COL_FLAGS)

ifneq ($(if $(DEBUG),$(strip $(DEBUG)),0),0)
	CFLAGS  = $(C_DEBUG_FLAGS)        $(C_OPENMP_FLAGS) $(C_OPTIONS_FLAGS) $(C_ARCH_FLAGS) $(C_INC_FLAGS) $(LIB_INC_PATH)
	LDFLAGS = $(C_DEBUG_FLAGS)        $(C_OPENMP_FLAGS) $(C_OPTIONS_FLAGS) $(C_ARCH_FLAGS) $(C_INC_FLAGS) $(LIB_LIB_PATH)
else
	CFLAGS  = $(C_OPTIMISATION_FLAGS) $(C_OPENMP_FLAGS) $(C_OPTIONS_FLAGS) $(C_ARCH_FLAGS) $(C_INC_FLAGS) $(LIB_INC_PATH)
	LDFLAGS = $(C_OPTIMISATION_FLAGS) $(C_OPENMP_FLAGS) $(C_OPTIONS_FLAGS) $(C_ARCH_FLAGS) $(C_INC_FLAGS) $(LIB_LIB_PATH)
endif

# -- Final product ----------
PRODUCT   = n-body$(SUFFIX)

# -- src and obj List ----------
SRC = $(addprefix ${SRC_PATH}/, $(FILE))
OBJ = $(addprefix ${OBJ_PATH}/, $(addsuffix $(SUFFIX).o, $(basename $(FILE))))

# -- Base rules ----------
$(OBJ_PATH)/%$(SUFFIX).o : $(SRC_PATH)/%.cpp
	$(CC) $(CFLAGS) -c $< -o $@
   
#-----Main rule ----------
$(EXE_PATH)/$(PRODUCT): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS) $(OPTFLAGS) $(CFG) $(INC) 

# -- Other stuff ----------
depend:
	makedepend $(CFLAGS) -Y $(SRC)

clean:
	rm -f $(OBJ_PATH)/*
	rm -f $(EXE_PATH)/*

