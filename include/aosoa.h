#pragma once

#include "real.h"
#include "constants.h"
#include "static_power.h"

#include <vector>
#include <array>
#include <cmath>

#define CLUSTER_SIZE 64
#include "index_remove.h"

#define AOSOA_FOR(iC, i, N, ...) {\
  int N_ITER##iC##i = N / CLUSTER_SIZE;\
  int iC;\
  for (iC = 0; iC < N_ITER##iC##i; ++iC) {\
    for (int i = 0; i < CLUSTER_SIZE; ++i) {\
      __VA_ARGS__\
    }\
  }\
  N_ITER##iC##i = N % CLUSTER_SIZE;\
  for (int i = 0; i < N_ITER##iC##i; ++i) {\
    __VA_ARGS__\
  }\
}

class PointCluster {
  public:
    //PointCluster()
    //{}
    //
    //PointCluster(const PointCluster& another)
    //  : _weight(another._weight), _radius(another._radius), _coord(another._coord), _momentum(another._momentum)
    //{}

    inline std::array<REAL, CLUSTER_SIZE>& weight() { return _weight; }
    inline const std::array<REAL, CLUSTER_SIZE>& weight() const { return _weight; }
    inline std::array<REAL, CLUSTER_SIZE>& radius() { return _radius; }
    inline const std::array<REAL, CLUSTER_SIZE>& radius() const { return _radius; }

    inline std::array<REAL, CLUSTER_SIZE>& coord(int k) { return _coord[k]; }
    inline const std::array<REAL, CLUSTER_SIZE>& coord(int k) const { return _coord[k]; }
    inline std::array<REAL, CLUSTER_SIZE>& momentum(int k) { return _momentum[k]; }
    inline const std::array<REAL, CLUSTER_SIZE>& momentum(int k) const { return _momentum[k]; }
  private:
    std::array<REAL, CLUSTER_SIZE> _weight __attribute__((aligned(CLUSTER_SIZE)));
    std::array<REAL, CLUSTER_SIZE> _radius __attribute__((aligned(CLUSTER_SIZE)));
    std::array<std::array<REAL, CLUSTER_SIZE>, DIM> _coord __attribute__((aligned(CLUSTER_SIZE)));
    std::array<std::array<REAL, CLUSTER_SIZE>, DIM> _momentum __attribute__((aligned(CLUSTER_SIZE)));
};

class Points {
  public:
    inline std::vector<PointCluster>& points() { return _points; }
    inline const std::vector<PointCluster>& points() const { return _points; }
    inline int size() const { return _size; }
    void resize(int s) {
      _size = s;
      points().resize(1 + s / CLUSTER_SIZE);
    }

    template <class RNG1, class RNG2, class RNG3, class RNG4>
    void random_init(int iC, int i, RNG1 rng_weight, RNG2 rng_radius, RNG3 rng_coord, RNG4 rng_momentum) {
      points()[iC].weight()[i] = rng_weight();
      points()[iC].radius()[i] = rng_radius();
      for (int k = 0; k < DIM; ++k) {
        points()[iC].coord(k)[i] = rng_coord();
        points()[iC].momentum(k)[i] = rng_momentum();
      }
    }

    inline bool force(int iC, int i, Points& others, int jC, int j, std::array<REAL, DIM>& force) {
      std::array<REAL, DIM> diff;
      REAL dist2 = 0;
      for (int k = 0; k < DIM; ++k) {
        diff[k] = others.points()[jC].coord(k)[j] - points()[iC].coord(k)[i];
      }
      for (int k = 0; k < DIM; ++k) {
        dist2 += diff[k] * diff[k];
      }
      REAL distDIM = sqrpower<DIM>(dist2);
#ifdef COL
      if (points()[iC].weight()[i] == 0 || others.points()[jC].weight()[j] == 0) return true;
      REAL cumul_radiusDIM = power<DIM>(points()[iC].radius()[i]) + power<DIM>(others.points()[jC].radius()[j]);
      if (distDIM <= cumul_radiusDIM) {
        REAL new_weight = points()[iC].weight()[i] + others.points()[jC].weight()[j];
        REAL new_radius = pow(cumul_radiusDIM, 1.0/DIM);
        REAL new_weight_rcp = 1.0 / new_weight;
        for (int k; k < DIM; ++k) {
          points()[iC].coord(k)[i] = (points()[iC].weight()[i] * points()[iC].coord(k)[i] + others.points()[jC].weight()[j] * others.points()[jC].coord(k)[j]) * new_weight_rcp;
          points()[jC].momentum(k)[i] = (points()[iC].weight()[i] * points()[iC].momentum(k)[i] + others.points()[jC].weight()[j] * others.points()[jC].momentum(k)[j]) * new_weight_rcp;
        }
        others.points()[jC].weight()[j] = new_weight;
        others.points()[jC].radius()[j] = new_radius;
        points()[iC].weight()[i] = 0;
        points()[iC].radius()[i] = 0;
        return false;
      }
#endif
      REAL g = points()[iC].weight()[i] * others.points()[jC].weight()[j] / distDIM;
      for (int k = 0; k < DIM; ++k) {
        force[k] += diff[k] * g;
      }
      return true;
    }

    inline void move(int iC, int i, const std::array<REAL, DIM>& force, REAL dt) {
      REAL dv = dt / points()[iC].weight()[i];
      for (int k = 0; k < DIM; ++k) {
        points()[iC].momentum(k)[i] += force[k] * dv;
      }
      for (int k = 0; k < DIM; ++k) {
        points()[iC].coord(k)[i] += points()[iC].momentum(k)[i] * dt;
      }
    }
  private:
    int _size;
    std::vector<PointCluster> _points;
};

class Simulation {
  public:
    Simulation()
      : _points(), _t(0), _dt(1)
    {}
    explicit Simulation(REAL dt)
      : _points(), _t(0), _dt(dt)
    {}

    inline Points& points() { return _points; }
    inline const Points& points() const { return _points; }
    inline Points& last_points() { return _last_points; }
    inline const Points& last_points() const { return _last_points; }
    inline REAL& t() { return _t; }
    inline REAL t() const { return _t; }
    inline REAL dt() const { return _dt; }

    template <class RNG1, class RNG2, class RNG3, class RNG4>
    void init(int n, RNG1 rng_weight, RNG2 rng_radius, RNG3 rng_coord, RNG4 rng_momentum) {
      points().resize(n);
      AOSOA_FOR(iC, i, n,
        points().random_init(iC, i, rng_weight, rng_radius, rng_coord, rng_momentum);
      )
      _t = 0;
    }

    void next_step() {
      int size = points().size();
      last_points().resize(size);
      std::swap(last_points(), points());
      std::array<REAL, DIM> force;
      
#ifdef COL
      std::vector<int> destroyed;
#endif
      
      AOSOA_FOR(iC, i, size,
        //std::fill(force.begin(), force.end(), 0);
        for (int k = 0; k < DIM; ++k) force[k] = 0;
        AOSOA_FOR(jC, j, size,
#ifndef COL
          points().force(iC, i, last_points(), jC, j, force);
#else
          if (!points().force(iC, i, last_points(), jC, j, force)) destroyed.push_back(i);
#endif
        )
        points().move(iC, i, force, dt());
      )

#ifdef COL
      //auto& weight = points().weight();
      //auto& radius = points().radius();
      //weight.erase(index_remove(weight.begin(), weight.end(), destroyed.begin(), destroyed.end()), weight.end());
      //radius.erase(index_remove(radius.begin(), radius.end(), destroyed.begin(), destroyed.end()), radius.end());
      //for (int k = 0; k < DIM; ++k) {
      //  auto& coord = points().coord(k);
      //  auto& momentum = points().momentum(k);
      //  coord.erase(index_remove(coord.begin(), coord.end(), destroyed.begin(), destroyed.end()), coord.end());
      //  momentum.erase(index_remove(momentum.begin(), momentum.end(), destroyed.begin(), destroyed.end()), momentum.end());
      //}
#endif

    }
  protected:
  private:
    Points _points, _last_points;
    REAL _t;
    REAL _dt;
};
