#pragma once

#include "real.h"
#include "constants.h"
#include "static_power.h"

#include "SOAContainer.h"

#include <vector>
#include <array>
#include <cmath>

#include "index_remove.h"

// Statically iterate over dimensions
template <unsigned n, int i, int incr>
struct APPLY_FOR {
  template <class F>
  static void call(F f) { f(std::integral_constant<int,i>()); APPLY_FOR<n-1, i+incr, incr>::call(f); }
};
template <int i, int incr>
struct APPLY_FOR<0, i, incr> {
  template <class F>
  static void call(F f) {}
};
#define FOR(i, begin, end, incr, ...) {APPLY_FOR<(end - begin) / incr, begin, incr>::call([&](auto APPLY_FOR_I) {constexpr int i = decltype(APPLY_FOR_I)::value; __VA_ARGS__});}
#define FOR_DIM(k, ...) FOR(k, 0, DIM, 1, __VA_ARGS__)


// Skin (Point object)
template <typename NAKEDPROXY>
class PointSkin : public NAKEDPROXY
{
  public:
    // convenience typedef
    typedef PointSkin<NAKEDPROXY> self_type;

    // forward constructor to underlying type
    template <typename... ARGS>
    PointSkin(ARGS&&... args) : NAKEDPROXY(std::forward<ARGS>(args)...) { }

    // forward (copy) assignment to underlying type
    template <typename ARG>
    self_type& operator=(const ARG& other)
    { NAKEDPROXY::operator=(other); return *this; }

    // forward (move) assignment to underlying type
    template <typename ARG>
    self_type& operator=(ARG&& other)
    { NAKEDPROXY::operator=(std::move(other)); return *this; }

    // Accessors
    inline REAL& weight() { return this->template get<0>(); }
    inline REAL weight() const { return this->template get<0>(); }
    inline REAL& radius() { return this->template get<1>(); }
    inline REAL radius() const { return this->template get<1>(); }
    template <int i> inline REAL& coord() { return this->template get<2+i>(); }
    template <int i> inline REAL coord() const { return this->template get<2+i>(); }
    template <int i> inline REAL& momentum() { return this->template get<2+DIM+i>(); }
    template <int i> inline REAL momentum() const { return this->template get<2+DIM+i>(); }

    template <class RNG1, class RNG2, class RNG3, class RNG4>
    void random_init(RNG1 rng_weight, RNG2 rng_radius, RNG3 rng_coord, RNG4 rng_momentum) {
      weight() = rng_weight();
      radius() = rng_radius();
      FOR_DIM(k,
        this->coord<k>() = rng_coord();
        this->momentum<k>() = rng_momentum();
      )
    }
    inline bool force(self_type another, std::array<REAL, DIM>& force) {
      std::array<REAL, DIM> diff;
      REAL dist2 = 0;
      FOR_DIM(k,
        diff[k] = another.coord<k>() - this->coord<k>();
      )
      FOR_DIM(k,
        dist2 += diff[k] * diff[k];
      )
      REAL distDIM = sqrpower<DIM>(dist2);
#ifdef COL
      if (weight() == 0 || another.weight() == 0) return true;
      REAL cumul_radiusDIM = power<DIM>(radius()) + power<DIM>(another.radius());
      if (distDIM <= cumul_radiusDIM) {
        REAL new_weight = weight() + another.weight();
        REAL new_radius = pow(cumul_radiusDIM, 1.0/DIM);
        REAL new_weight_rcp = 1.0 / new_weight;
        FOR_DIM(k,
          this->coord<k>() = (this->weight() * this->coord<k>() + another.weight() * another.coord<k>()) * new_weight_rcp;
          this->momentum<k>() = (this->weight() * this->momentum<k>() + another.weight() * another.momentum<k>()) * new_weight_rcp;
        )
        another.weight() = new_weight;
        another.radius() = new_radius;
        weight() = 0;
        radius() = 0;
        return false;
      }
#endif
      REAL g = weight() * another.weight() / distDIM;
      FOR_DIM(k,
        force[k] += diff[k] * g;
      )
      return true;
    }

    inline void move(const std::array<REAL, DIM>& force, REAL dt) {
      REAL dv = dt / weight();
      FOR_DIM(k,
        this->momentum<k>() += force[k] * dv;
      )
      FOR_DIM(k,
        this->coord<k>() += this->momentum<k>() * dt;
      )
    }
};

// Helper for arbitrary multidimensionnal SOAContainer
template <unsigned N, class... Args>
struct SOAContainerGenerator {
  typedef typename SOAContainerGenerator<N - 1, REAL, REAL, Args...>::container container;
};
template <class... Args>
struct SOAContainerGenerator<0, Args...> {
  typedef SOAContainer<std::vector, PointSkin, REAL, REAL, Args...> container;
};

// Typedefs
typedef typename SOAContainerGenerator<DIM>::container Points;
typedef typename Points::reference Point;

class Simulation {
  public:
    Simulation()
      : _points(), _t(0), _dt(1)
    {}
    explicit Simulation(REAL dt)
      : _points(), _t(0), _dt(dt)
    {}

    inline Points& points() { return _points; }
    inline const Points& points() const { return _points; }
    inline Points& last_points() { return _last_points; }
    inline const Points& last_points() const { return _last_points; }
    inline REAL& t() { return _t; }
    inline REAL t() const { return _t; }
    inline REAL dt() const { return _dt; }

    template <class RNG1, class RNG2, class RNG3, class RNG4>
    void init(int n, RNG1 rng_weight, RNG2 rng_radius, RNG3 rng_coord, RNG4 rng_momentum) {
      points().resize(n);
      for (Point point : points()) {
        point.random_init(rng_weight, rng_radius, rng_coord, rng_momentum);
      }
      _t = 0;
    }

    void next_step() {
      int size = points().size();
      last_points().resize(size);
      std::swap(last_points(), points());
      std::array<REAL, DIM> force;

#ifdef COL
      std::vector<int> destroyed;
#endif
      
      for (int i = 0; i < size; ++i) {
        Point p1 = points()[i];
        //std::fill(force.begin(), force.end(), 0);
        for (int k = 0; k < DIM; ++k) force[k] = 0;
        for (int j = 0; j < size; ++j) {
          Point p2 = last_points()[j];
#ifndef COL
          p1.force(p2, force);
#else
          if (!p1.force(p2, force)) destroyed.push_back(i);
#endif
        }
        p1.move(force, dt());
      }

#ifdef COL
      points().erase(index_remove(points().begin(), points().end(), destroyed.begin(), destroyed.end()), points().end());
#endif

    }
  protected:
  private:
    Points _points, _last_points;
    REAL _t;
    REAL _dt;
};
