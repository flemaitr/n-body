#pragma once

#include "real.h"
#include "constants.h"
#include <functional>
#include <chrono>
#include <random>

auto rng(unsigned int seed = 0)
  -> decltype(std::mt19937())
{
  return std::mt19937(seed);
}
auto rng_t()
  -> decltype(rng())
{
  unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
  return rng(seed);
}

template <class RNG>
auto normal_rng(RNG rng, REAL mean, REAL stddev)
  -> decltype(std::bind(std::normal_distribution<REAL>(), rng))
{
  std::normal_distribution<REAL> norm(mean, stddev);
  return std::bind(norm, rng);
}

template <class RNG>
auto weight_rng(RNG rng)
  -> decltype(normal_rng(rng, 0, 0))
{
  return normal_rng(rng, 100, 10);
}

template <class RNG>
auto radius_rng(RNG rng)
  -> decltype(normal_rng(rng, 0, 0))
{
  return normal_rng(rng, 100, 10);
}

template <class RNG>
auto coord_rng(RNG rng)
  -> decltype(normal_rng(rng, 0, 0))
{
  return normal_rng(rng, 0, 100);
}

template <class RNG>
auto momentum_rng(RNG rng)
  -> decltype(normal_rng(rng, 0, 0))
{
  return normal_rng(rng, 0, 0);
}
