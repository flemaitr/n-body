#pragma once

#include "real.h"
#include "constants.h"
#include "static_power.h"

#include <vector>
#include <array>
#include <cmath>

#include "index_remove.h"


class Points {
  public:
    std::vector<REAL>& weight() { return _weight; }
    const std::vector<REAL>& weight() const { return _weight; }
    std::vector<REAL>& radius() { return _radius; }
    const std::vector<REAL>& radius() const { return _radius; }
    std::vector<REAL>& coord(int i) { return _coord[i]; }
    const std::vector<REAL>& coord(int i) const { return _coord[i]; }
    std::vector<REAL>& momentum(int i) { return _momentum[i]; }
    const std::vector<REAL>& momentum(int i) const { return _momentum[i]; }

    inline int size() const { return _size; }
    void resize(int s) {
      _size = s;
      weight().resize(s);
      radius().resize(s);
      for (int i = 0; i < DIM; ++i) {
        coord(i).resize(s);
        momentum(i).resize(s);
      }
    }

    template <class RNG1, class RNG2, class RNG3, class RNG4>
    void random_init(int i, RNG1 rng_weight, RNG2 rng_radius, RNG3 rng_coord, RNG4 rng_momentum) {
      weight()[i] = rng_weight();
      radius()[i] = rng_radius();
      for (int k = 0; k < DIM; ++k) {
        coord(k)[i] = rng_coord();
        momentum(k)[i] = rng_momentum();
      }
    }

    inline bool force(int i, Points& points, int j, std::array<REAL, DIM>& force) {
      std::array<REAL, DIM> diff;
      REAL dist2 = 0;
      for (int k = 0; k < DIM; ++k) {
        diff[k] = points.coord(k)[j] - coord(k)[i];
      }
      for (int k = 0; k < DIM; ++k) {
        dist2 += diff[k] * diff[k];
      }
      REAL distDIM = sqrpower<DIM>(dist2);
#ifdef COL
      if (weight()[i] == 0 || points.weight()[j] == 0) return true;
      REAL cumul_radiusDIM = power<DIM>(radius()[i]) + power<DIM>(points.radius()[j]);
      if (distDIM <= cumul_radiusDIM) {
        REAL new_weight = weight()[i] + points.weight()[j];
        REAL new_radius = pow(cumul_radiusDIM, 1.0/DIM);
        REAL new_weight_rcp = 1.0 / new_weight;
        for (int k; k < DIM; ++k) {
          coord(k)[i] = (weight()[i] * coord(k)[i] + points.weight()[j] * points.coord(k)[j]) * new_weight_rcp;
          momentum(k)[i] = (weight()[i] * momentum(k)[i] + points.weight()[j] * points.momentum(k)[j]) * new_weight_rcp;
        }
        points.weight()[j] = new_weight;
        points.radius()[j] = new_radius;
        weight()[i] = 0;
        radius()[i] = 0;
        return false;
      }
#endif
      REAL g = weight()[i] * points.weight()[j] / distDIM;
      for (int k = 0; k < DIM; ++k) {
        force[k] += diff[k] * g;
      }
      return true;
    }

    inline void move(int i, const std::array<REAL, DIM>& force, REAL dt) {
      REAL dv = dt / weight()[i];
      for (int k = 0; k < DIM; ++k) {
        momentum(k)[i] += force[k] * dv;
      }
      for (int k = 0; k < DIM; ++k) {
        coord(k)[i] += momentum(k)[i] * dt;
      }
    }
  private:
    int _size;
    std::vector<REAL> _weight;
    std::vector<REAL> _radius;
    std::array<std::vector<REAL>, DIM> _coord;
    std::array<std::vector<REAL>, DIM> _momentum;
};


class Simulation {
  public:
    Simulation()
      : _points(), _t(0), _dt(1)
    {}
    explicit Simulation(REAL dt)
      : _points(), _t(0), _dt(dt)
    {}

    inline Points& points() { return _points; }
    inline const Points& points() const { return _points; }
    inline Points& last_points() { return _last_points; }
    inline const Points& last_points() const { return _last_points; }
    inline REAL& t() { return _t; }
    inline REAL t() const { return _t; }
    inline REAL dt() const { return _dt; }

    template <class RNG1, class RNG2, class RNG3, class RNG4>
    void init(int n, RNG1 rng_weight, RNG2 rng_radius, RNG3 rng_coord, RNG4 rng_momentum) {
      points().resize(n);
      for (int i = 0; i < n; ++i) {
        points().random_init(i, rng_weight, rng_radius, rng_coord, rng_momentum);
      }
      _t = 0;
    }

    void next_step() {
      int size = points().size();
      last_points().resize(size);
      std::swap(last_points(), points());
      std::array<REAL, DIM> force;
      
#ifdef COL
      std::vector<int> destroyed;
#endif
      
      for (int i = 0; i < size; ++i) {
        //std::fill(force.begin(), force.end(), 0);
        for (int k = 0; k < DIM; ++k) force[k] = 0;
        for (int j = 0; j < size; ++j) {
#ifndef COL
          points().force(i, last_points(), j, force);
#else
          if (!points().force(i, last_points(), j, force)) destroyed.push_back(i);
#endif
        }
        points().move(i, force, dt());
      }

#ifdef COL
      auto& weight = points().weight();
      auto& radius = points().radius();
      weight.erase(index_remove(weight.begin(), weight.end(), destroyed.begin(), destroyed.end()), weight.end());
      radius.erase(index_remove(radius.begin(), radius.end(), destroyed.begin(), destroyed.end()), radius.end());
      for (int k = 0; k < DIM; ++k) {
        auto& coord = points().coord(k);
        auto& momentum = points().momentum(k);
        coord.erase(index_remove(coord.begin(), coord.end(), destroyed.begin(), destroyed.end()), coord.end());
        momentum.erase(index_remove(momentum.begin(), momentum.end(), destroyed.begin(), destroyed.end()), momentum.end());
      }
#endif

    }
  protected:
  private:
    Points _points, _last_points;
    REAL _t;
    REAL _dt;
};
