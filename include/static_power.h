#pragma once
#include <cmath>


template <int P>
double power(double a, double a2) {
  if (P == 0) return 1.0;
  if (P == 1) return a;
  if (P == 2) return a2;
  if (P < 0) return 1.0 / power<-P>(a, a2);
  if (P % 2 == 0) return power<P / 2>(a2, a2*a2);
  return a * power<(P - 1) / 2>(a2, a2*a2);
}

template <int P>
double power(double a) {
  return power<P>(a, a*a);
}

template <int P>
double sqrpower(double a2) {
  if (P < 0) return 1.0 / sqrpower<-P>(a2);
  if (P % 2 == 1) return power<P>(sqrt(a2), a2);
  return power<P / 2>(a2);
}
