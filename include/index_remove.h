#pragma once

template <class I1, class I2>
I1 index_remove(I1 first, I1 last, I2 first_index, I2 last_index, int offset = 0) {
  int i = offset;
  I1 from = first;
  I1 to = first;
  I2 index = first_index;
  bool move = false;
  while (index != last_index && *index < i) ++index;
  while (from != last && index != last_index) {
    if (*index == i) {
      ++index;
      move = true;
    } else {
      if (move) *to = std::move(*from);
      ++to;
    }
    ++from;
    ++i;
  }
  if (!move) return last;
  while (from != last) {
    *to = std::move(*from);
    ++from;
    ++to;
  }
  return to;
}
