#pragma once

#include "real.h"
#include "constants.h"
#include "static_power.h"

#include <vector>
#include <array>
#include <memory>
#include <cmath>

#include "index_remove.h"

class Point {
  public:
    Point()
    {}
    explicit Point(REAL w)
      : _weight(w)
    {}
    Point(const Point& another)
      : _weight(another._weight), _coord(another._coord), _momentum(another._momentum)
    {}

    inline REAL& weight() { return _weight; }
    inline REAL weight() const { return _weight; }
    inline REAL& radius() { return _radius; }
    inline REAL radius() const { return _radius; }

    inline REAL& coord(int k) { return _coord[k]; }
    inline REAL coord(int k) const { return _coord[k]; }

    inline REAL& momentum(int k) { return _momentum[k]; }
    inline REAL momentum(int k) const { return _momentum[k]; }

    template <class RNG1, class RNG2, class RNG3, class RNG4>
    void random_init(RNG1 rng_weight, RNG2 rng_radius, RNG3 rng_coord, RNG4 rng_momentum) {
      _weight = rng_weight();
      _radius = rng_radius();
      for (int k = 0; k < DIM; ++k) {
        coord(k) = rng_coord();
        momentum(k) = rng_momentum();
      }
    }

    inline bool force(Point& another, std::array<REAL, DIM>& force) {
      std::array<REAL, DIM> diff;
      REAL dist2 = 0;
      for (int k = 0; k < DIM; ++k) {
        diff[k] = another.coord(k) - coord(k);
      }
      for (int k = 0; k < DIM; ++k) {
        dist2 += diff[k] * diff[k];
      }
      REAL distDIM = sqrpower<DIM>(dist2);
#ifdef COL
      if (weight() == 0 || another.weight() == 0) return true;
      REAL cumul_radiusDIM = power<DIM>(radius()) + power<DIM>(another.radius());
      if (distDIM <= cumul_radiusDIM) {
        REAL new_weight = weight() + another.weight();
        REAL new_radius = pow(cumul_radiusDIM, 1.0/DIM);
        REAL new_weight_rcp = 1.0 / new_weight;
        for (int k; k < DIM; ++k) {
          coord(k) = (weight() * coord(k) + another.weight() * another.coord(k)) * new_weight_rcp;
          momentum(k) = (weight() * momentum(k) + another.weight() * another.momentum(k)) * new_weight_rcp;
        }
        another.weight() = new_weight;
        another.radius() = new_radius;
        weight() = 0;
        radius() = 0;
        return false;
      }
#endif
      REAL g = weight() * another.weight() / distDIM;
      for (int k = 0; k < DIM; ++k) {
        force[k] += diff[k] * g;
      }
      return true;
    }

    inline void move(const Point& before, const std::array<REAL, DIM>& force, REAL dt) {
      REAL dv = dt / weight();
      for (int k = 0; k < DIM; ++k) {
        momentum(k) = before.momentum(k) + force[k] * dv;
      }
      for (int k = 0; k < DIM; ++k) {
        coord(k) = before.coord(k) + momentum(k) * dt;
      }
    }
  private:
    REAL _weight;
    REAL _radius;
    std::array<REAL, DIM> _coord;
    std::array<REAL, DIM> _momentum;
};

typedef std::vector<std::unique_ptr<Point>> Points;

class Simulation {
  public:
    Simulation()
      : _points(), _t(0), _dt(1)
    {}
    explicit Simulation(REAL dt)
      : _points(), _t(0), _dt(dt)
    {}

    inline Points& points() { return _points; }
    inline const Points& points() const { return _points; }
    inline Points& last_points() { return _last_points; }
    inline const Points& last_points() const { return _last_points; }
    inline REAL& t() { return _t; }
    inline REAL t() const { return _t; }
    inline REAL dt() const { return _dt; }

    template <class RNG1, class RNG2, class RNG3, class RNG4>
    void init(int n, RNG1 rng_weight, RNG2 rng_radius, RNG3 rng_coord, RNG4 rng_momentum) {
      points().resize(n);
      last_points().resize(n);
      for (int i = 0; i < n; ++i) {
        std::unique_ptr<Point> ptr(new Point());
        ptr->random_init(rng_weight, rng_radius, rng_coord, rng_momentum);
        std::unique_ptr<Point> ptr2(new Point(*ptr));
        points()[i] = std::move(ptr);
        last_points()[i] = std::move(ptr2);
      }
      _t = 0;
    }

    void next_step() {
      int size = points().size();
      last_points().resize(size);
      std::swap(last_points(), points());
      std::array<REAL, DIM> force;

#ifdef COL
      std::vector<int> destroyed;
#endif
      
      for (int i = 0; i < size; ++i) {
        Point& p1 = *last_points()[i];
        //std::fill(force.begin(), force.end(), 0);
        for (int k = 0; k < DIM; ++k) force[k] = 0;
        for (int j = 0; j < size; ++j) {
          Point& p2 = *last_points()[j];
#ifndef COL
          p1.force(p2, force);
#else
          if (!p1.force(p2, force)) destroyed.push_back(i);
#endif
        }
        points()[i]->move(p1, force, dt());
      }

#ifdef COL
      points().erase(index_remove(points().begin(), points().end(), destroyed.begin(), destroyed.end()), points().end());
      last_points().erase(index_remove(last_points().begin(), last_points().end(), destroyed.begin(), destroyed.end()), last_points().end());
#endif

    }
  protected:
  private:
    Points _points, _last_points;
    REAL _t;
    REAL _dt;
};
