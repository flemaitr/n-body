#include <iostream>
#include <cstring>
#include <iomanip>
#include "rng.h"
#include "rdtsc.h"
#include "realtime.h"


#define _stringify(x) #x
#define stringify(x) _stringify(x)
#define my_header(x) stringify(x.h)
#include my_header(MLAYOUT)

int getArgNum(int argc, char* argv[], int i, int defaultNum) {
  if (i >= argc) {
    return defaultNum;
  }
  char* p;
  char* arg = argv[i];
  double res;
  if (sscanf(arg, "%lf", &res)) {
    return (int) res;
  } else {
    return defaultNum;
  }
}


int main(int argc, char* argv[]) {
  
  int Npoints = getArgNum(argc, argv, 1, 100);
  int Niter = getArgNum(argc, argv, 2, 100);


  auto mt_rng = rng();
  auto weightG = weight_rng(mt_rng);
  auto radiusG = radius_rng(mt_rng);
  auto coordsG = coord_rng(mt_rng);
  auto momentumG = momentum_rng(mt_rng);

  std::cerr << "Memory Layout: " << stringify(MLAYOUT) << std::endl;

  Simulation simulation(1);
  simulation.init(Npoints, weightG, radiusG, coordsG, momentumG);

  std::cerr << Npoints << " points   " << Niter << " iterations   " << DIM << " dimensions" << std::endl;
  double time = static_cast<double>(getRealTime());
  double ctime = static_cast<double>(rdtsc());
  for (int i = 0; i < Niter; ++i) {
    simulation.next_step();
  }

  double normalization = static_cast<double>(Niter) * static_cast<double>(Npoints) * static_cast<double>(Npoints);
  ctime = static_cast<double>(rdtsc()) - ctime;
  time = static_cast<double>(getRealTime()) - time;
  //std::cout << std::setw(15) << ctime                               << " cycles"                   << std::endl;
  std::cout << std::setw(15) << time                                << "    sec"                   << std::endl;
  //std::cout << std::setw(15) << ctime / double(Niter)               << " cycles.iter⁻¹"            << std::endl;
  std::cout << std::setw(15) << time  / double(Niter)               << "    sec.iter⁻¹"            << std::endl;
  //std::cout << std::setw(15) << ctime / normalization               << " cycles.iter⁻¹.el⁻²"       << std::endl;
  std::cout << std::setw(15) << time  / normalization               << "    sec.iter⁻¹.el⁻²"       << std::endl;
  //std::cout << std::setw(15) << ctime / normalization / double(DIM) << " cycles.iter⁻¹.el⁻².dim⁻¹" << std::endl;
  std::cout << std::setw(15) << time  / normalization / double(DIM) << "    sec.iter⁻¹.el⁻².dim⁻¹" << std::endl;
  return 0;
}
